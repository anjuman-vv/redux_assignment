
const redux = require('redux')
const reduxLogger = require('redux-logger')
const createStore = redux.createStore
const combineReducers = redux.combineReducers
const applyMiddleware = redux.applyMiddleware
const logger = reduxLogger.createLogger()

const BUY_FOOD = 'BUY_FOOD'
const BUY_SWEET = 'BUY_SWEET'

function buyFood () {
  return {
    type: BUY_FOOD,
    info: 'First redux action'
  }
}

function buySweet () {
  return {
    type: BUY_SWEET
  }
}



const initialFoodState = {
  numOfFoods: 10
}


const initialSweetState = {
  numofSweets: 20
}


const foodReducer = (state = initialFoodState, action) => {
  switch (action.type) {
    case BUY_FOOD: return {
      ...state,
      numOfFoods: state.numOfFoods - 1
    }
    default: return state
  }
}

const iceCreamReducer = (state = initialSweetState, action) => {
  switch (action.type) {
    case BUY_SWEET: return {
      ...state,
      numofSweets: state.numofSweets - 1
    }
    default: return state
  }
}

const rootReducer = combineReducers({
  food: foodReducer,
  sweet: iceCreamReducer
})
const store = createStore(rootReducer, applyMiddleware(logger))
console.log('Initial State ', store.getState())
const unsubscribe = store.subscribe(() => { })
store.dispatch(buyFood())
store.dispatch(buyFood())
store.dispatch(buyFood())
store.dispatch(buySweet())
store.dispatch(buySweet())
unsubscribe()


//